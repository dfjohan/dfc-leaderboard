function dbPut(key, value) {
  localStorage[key] = JSON.stringify(value);
  localStorage['lastChange'] = new Date().getTime();
}

function dbGet(key, defaultvalue) {
  if (localStorage[key] !== undefined) {
    return JSON.parse(localStorage[key]);
  } else if (defaultvalue !== undefined) {
    return defaultvalue;
  }
  return null;
}

function dbRemove(key) {
  localStorage.removeItem(key);
  localStorage['lastChange'] = new Date().getTime();
}

var _t = {};
//Compile all templates on start
$('script[type="text/x-handlebars-template"]').each(
  function() {
    var name = $(this).attr('id');
    _t[name] = Handlebars.compile($(this).html());
  }
);


/*
 * App states:
 *  - leaderboard
 *  - contest
 */

$(document).ready(
  function() {
    $('#add-contestant').on('click', toggleAddBox);
    $('form.contestant').on('submit',
      function(e) {
        addNewContestant();
        $('form.contestant').find('.cancel').click();
        e.preventDefault();
        return false;
      }
    );
    $('form.contestant').find('.cancel').on('click',
      function() {
        toggleAddBox();
      }
    );

    $('.contestcontrols .start').on('click',
      function() {
        var $this = $(this);
        if (!$this.hasClass('green'))
          return;
        dbPut('appState', 'contest');
        $('.contestcontrols .start').removeClass('green');
        $('.contestcontrols .cancel').addClass('red');
        $('.won').removeClass('won');
      }
    );
    $('.contestcontrols .setwinner').on('click',
      function() {
        var $this = $(this);
        if (!$this.hasClass('green'))
          return;
        var id = dbGet('winner');
        var data = dbGet(id);
        data.wins++;
        dbPut(id, data);
        dbPut('appState', 'leaderboard');
        removeLeftContestant();
        removeRightContestant();
        createContestantList();
      }
    );
    $('.contestcontrols .cancel').on('click',
      function() {
        var $this = $(this);
        if (!$this.hasClass('red'))
          return;
        removeWinner();
        dbPut('appState', 'leaderboard');
        $('.contestcontrols .start').addClass('green');
        $('.contestcontrols .setwinner').removeClass('green');
        $('.contestcontrols .cancel').removeClass('red');
      }
    );

    var state = dbGet('appState');
    var left = dbGet('leftContestant');
    var right = dbGet('rightContestant');

    if (left)
      setLeftContestant(left);
    if (right)
      setRightContestant(right);

    if (state == 'contest') {
      $('.contestcontrols .start').removeClass('green');
      $('.contestcontrols .setwinner').removeClass('green');
      $('.contestcontrols .cancel').addClass('red');
    }

    createContestantList();
  }
);


function toggleAddBox() {
  var $box = $('form.contestant');
  if ($box.is(':visible'))
    $box.fadeOut();
  else
    $box.fadeIn();
}

var globalCounter = 0;

function addNewContestant() {
  var valArray = $('form.contestant').serializeArray();
  var values = {};
  for (var i in valArray) {
    values[valArray[i].name] = valArray[i].value;
  }
  values['wins'] = 0;//Math.floor(Math.random()*20);
  var timestamp = new Date().getTime();
  values['lastUpdated'] = timestamp;
  values['id'] = timestamp+''+globalCounter++;
  var list = dbGet('contestants', []);
  list.push(values['id']);

  dbPut(values['id'], values);
  dbPut('contestants', list);
  createContestantList();
}

function createContestantList() {
  var list = dbGet('contestants', []);
  var results = [];
  for (var i in list) {
    var item = dbGet(list[i]);
    if (item)
      results.push(item);
  }
  results.sort(
    function(a,b) {
      var property = 'wins';
      var dir = -1;
      var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
      return result * dir;
    }
  );
  $listing = $('.contestant-listing');
  $listing.html('');
  var pos = 0;
  for (i in results) {
    item = results[i];
    item['position'] = ++pos;
    var html = _t['contestantitem'](item);
    $listing.append(html);
  }

  attachListingEvents();
}

function attachListingEvents() {
  var items = $('.contestant-listing');
  items.find('.controls .edit').on('click',
    function() {
      var id = $(this).attr('data-id');
      createEditControls(id);
    }
  );
  items.find('.controls .left').on('click',
    function() {
      var state = dbGet('appState', 'leaderboard');
      if (state != 'leaderboard')
        return;
      var id = $(this).attr('data-id');
      setLeftContestant(id);
    }
  );
  items.find('.controls .right').on('click',
    function() {
      var state = dbGet('appState', 'leaderboard');
      if (state != 'leaderboard')
        return;
      var id = $(this).attr('data-id');
      setRightContestant(id);
    }
  );
}

function createEditControls(id) {
  var $container = $('li[data-id="'+id+'"]');
  var data = dbGet(id);
  if (data) {
    var html = _t['editcontestant'](data);
    $container.append(html);
  }

  var $form = $container.find('form.editcontestant');
  $form.on('submit',
    function(e) {
      var valArray = $form.serializeArray();
      var values = {};
      for (var i in valArray) {
        values[valArray[i].name] = valArray[i].value;
      }
      values['wins'] = parseInt(values['wins']);
      values['lastUpdated'] = new Date().getTime();

      dbPut(values['id'], values);
      createContestantList();

      e.preventDefault();
      return false;
    }
  );
  $form.find('.cancel').on('click',
    function() {
      $form.remove();
    }
  );
  $form.find('.delete').on('click',
    function(e) {
      e.preventDefault();
      var $this = $(this);
      if ($this.hasClass('orange')) {
        $this.removeClass('orange').addClass('red');
        return false;
      }
      removeContestant(id);
    }
  );

}

function removeContestant(id) {
  var list = dbGet('contestants', []);
  for (var i = 0; i < list.length; i++) {
    if (list[i].id == id) {
      list.splice(i, 1);
      break;
    }
  }
  dbRemove(id);
  createContestantList();
}


function setLeftContestant(id) {
  if (getRightContestant() == id)
    removeRightContestant();
  if (getRightContestant()) {
    $('.contestcontrols .start').addClass('green');
    $('.contestcontrols .setwinner').removeClass('green');
    $('.contestcontrols .cancel').removeClass('red');
  }
  var $container = $('#nextcontest .contesters.left');
  var data = dbGet(id);
  dbPut('leftContestant', id);
  if (data) {
    var html = _t['contesteritem'](data);
    $container.html(html);
  }
  $container.find('.winner').on('click',
    function() {
      $parent = $container.find('>div');
      if ($parent.hasClass('won')) {
        $parent.removeClass('won');
        removeWinner(id);
      } else {
        $('.won').removeClass('won');
        $parent.addClass('won');
        setWinner(id);
      }
    }
  );
}

function setRightContestant(id) {
  if (getLeftContestant() == id)
    removeLeftContestant();
  if (getLeftContestant()) {
    $('.contestcontrols .start').addClass('green');
    $('.contestcontrols .setwinner').removeClass('green');
    $('.contestcontrols .cancel').removeClass('red');
  }
  var $container = $('#nextcontest .contesters.right');
  var data = dbGet(id);
  dbPut('rightContestant', id);
  if (data) {
    var html = _t['contesteritem'](data);
    $container.html(html);
  }
  $container.find('.winner').on('click',
    function() {
      $parent = $container.find('>div');
      if ($parent.hasClass('won')) {
        $parent.removeClass('won');
        removeWinner(id);
      } else {
        $('.won').removeClass('won');
        $parent.addClass('won');
        setWinner(id);
      }
    }
  );
}

function getLeftContestant() {
  var $container = $('#nextcontest .contesters.left > div');
  return $container.attr('data-id');
}
function getRightContestant() {
  var $container = $('#nextcontest .contesters.right > div');
  return $container.attr('data-id');
}

function removeLeftContestant() {
  $('#nextcontest .contesters.left').html('');
  dbRemove('leftContestant');
  $('.contestcontrols .start').removeClass('green');
  $('.contestcontrols .setwinner').removeClass('green');
  $('.contestcontrols .cancel').removeClass('red');
}
function removeRightContestant() {
  $('#nextcontest .contesters.right').html('');
  dbRemove('rightContestant');
  $('.contestcontrols .start').removeClass('green');
  $('.contestcontrols .setwinner').removeClass('green');
  $('.contestcontrols .cancel').removeClass('red');
}


function setWinner(id) {
  dbPut('winner', id);
  var state = dbGet('appState');
  if (state == 'contest')
  $('.contestcontrols .setwinner').addClass('green');
}

function removeWinner() {
  dbRemove('winner');
}

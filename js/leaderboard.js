$(document).ready(
  function() {
    $('section').click(
      function() {
        enterFullscreen();
      }
    );

    window.addEventListener('storage', handleStorageChange);

    start();
  }
);

var appState = dbGet('appState', 'leaderboard');
var _lastChange = dbGet('lastChange', 0);


function switchToContestants() {
  $('#versus').not(':visible').fadeIn();
}

function switchToLeaderboard() {
  $('#versus:visible').fadeOut();
}

function start() {
  updateLeaderboard();
  var state = dbGet('appState', 'leaderboard');
  if (state == 'contest') {
    updateContestants();
    switchToContestants();
  }
  runLoop();
}


function runLoop() {
  var lastChange = dbGet('lastChange', 0);
  if (lastChange != _lastChange) {
    _lastChange = lastChange;
    var state = dbGet('appState', 'leaderboard');
    if (state == 'leaderboard') {
      updateLeaderboard();
      switchToLeaderboard();
    } else if (state == 'contest') {
      updateContestants();
      switchToContestants();
    }
  }
  setTimeout(runLoop, 200);
}

function updateLeaderboard(callback) {
  var list = getLeaderboardData();
  var $board = $('.toplist');
  $board.html('');
  for (var i in list) {
    var item = list[i];
    var html = _t['leaderboarditem'](item);
    $board.append(html);
  }
  if (callback && typeof callback == 'function')
    callback();
}

function updateContestants(callback) {
  var leftid = dbGet('leftContestant');
  var rightid = dbGet('rightContestant');
  var left = dbGet(leftid);
  var right = dbGet(rightid);

  var html = _t['contestitem'](left);
  $('#versus .left').html(html);
  html = _t['contestitem'](right);
  $('#versus .right').html(html);

  if (callback && typeof callback == 'function')
    callback();
}


function getLeaderboardData() {
  var list = dbGet('contestants', []);
  var results = [];
  for (var i in list) {
    var item = dbGet(list[i]);
    if (item)
      results.push(item);
  }
  results.sort(
    function(a,b) {
      var property = 'wins';
      var dir = -1;
      var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
      return result * dir;
    }
  );
  results = results.splice(0, 5);
  for (var p = 0; p < 5 && p < results.length; p++) {
    results[p]['position'] = p+1;
  }
  return results;
}

function handleStorageChange(e) {
}


/*////////////////////////////////////*/
/**
 * Helper functions for entering and exiting fullscreen mode
 * Source: https://developer.mozilla.org/en-US/docs/Web/Guide/API/DOM/Using_full_screen_mode?redirectlocale=en-US&redirectslug=Web%2FGuide%2FDOM%2FUsing_full_screen_mode
 */
/*////////////////////////////////////*/


function toggleFullscreen() {
  if (!document.fullscreenElement &&    // alternative standard method
      !document.mozFullScreenElement && !document.webkitFullscreenElement) {  // current working methods
    enterFullscreen();
  } else {
    exitFullscreen();
  }
}

function enterFullscreen() {
  if (!document.fullscreenElement && !document.mozFullScreenElement && !document.webkitFullscreenElement) {
    if (document.documentElement.requestFullscreen) {
      document.documentElement.requestFullscreen();
    } else if (document.documentElement.mozRequestFullScreen) {
      document.documentElement.mozRequestFullScreen();
    } else if (document.documentElement.webkitRequestFullscreen) {
      document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
    }
  }
}

function exitFullscreen() {
  if (document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement) {
    if (document.cancelFullScreen) {
      document.cancelFullScreen();
    } else if (document.mozCancelFullScreen) {
      document.mozCancelFullScreen();
    } else if (document.webkitCancelFullScreen) {
      document.webkitCancelFullScreen();
    }
  }
}
